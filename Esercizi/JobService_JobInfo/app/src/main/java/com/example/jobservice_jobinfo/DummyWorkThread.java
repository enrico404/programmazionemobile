package com.example.jobservice_jobinfo;

import android.app.job.JobParameters;
import android.app.job.JobService;
import android.util.Log;

import java.util.Random;

public class DummyWorkThread extends Thread{
    private int conta;
    private boolean canRun;
    private Random rnd;
    private JobService job;
    private JobParameters params;



    public DummyWorkThread(JobService job, JobParameters params) {
        canRun = true;
        rnd = new Random(System.currentTimeMillis());
        this.job = job;
        this.params = params;
        conta = params.getExtras().getInt("conta");



    }

    public boolean canRun(){return canRun;}

    @Override
    public void run(){

        synchronized (this){
            canRun = true;
        }

        int sleepTime;

        try{
            while (conta < 100 && canRun){
                sleepTime = rnd.nextInt(2000);
                Thread.sleep(sleepTime);
                conta ++;
                Log.d("TAG","Scrivo " + conta );
            }

        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        if (conta ==100){

            job.jobFinished(params, false);

        }

    }

    public synchronized void stopRunning() {
        canRun = false;
    }
}
