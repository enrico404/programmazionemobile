package com.example.jobservice_jobinfo;

import androidx.appcompat.app.AppCompatActivity;

import android.app.Activity;
import android.app.job.JobInfo;
import android.app.job.JobScheduler;
import android.content.ComponentName;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Switch;
import android.widget.Toast;

import static android.content.Context.JOB_SCHEDULER_SERVICE;

public class MainActivity extends Activity implements View.OnClickListener {

    public static final int JOB_ID = 100100;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Button b = findViewById(R.id.button);
        b.setOnClickListener(this);

        cancellaTask();
    }

    @Override
    public void onClick(View v) {
        int netType = JobInfo.NETWORK_TYPE_NONE;
        boolean reqCharging = false;
        Switch swB = findViewById(R.id.switch1);
        Switch swC = findViewById(R.id.switch2);

        if(swB.isChecked()){
            reqCharging = true;
        }

        if(swC.isChecked()){
            netType = JobInfo.NETWORK_TYPE_ANY;
        }


        JobInfo.Builder jobB = new JobInfo.Builder(JOB_ID, new ComponentName(this, ServizioBackground.class));

        jobB.setRequiredNetworkType(netType);
        jobB.setRequiresCharging(reqCharging);
        //basta che ci sia uno dei due
        jobB.setBackoffCriteria(100, JobInfo.BACKOFF_POLICY_LINEAR);

        JobInfo job = jobB.build();
        job.getExtras().putInt("conta", 0);

        JobScheduler scheduler = (JobScheduler)getSystemService(JOB_SCHEDULER_SERVICE);
        if(scheduler.schedule(job) != JobScheduler.RESULT_SUCCESS){
            Toast.makeText(this, "Errore in scheduling", Toast.LENGTH_LONG).show();
        }else
            Toast.makeText(this, "Job schedulato", Toast.LENGTH_LONG).show();

    }

    private void cancellaTask(){
        JobScheduler scheduler = (JobScheduler) getSystemService(JOB_SCHEDULER_SERVICE);
         scheduler.cancel(JOB_ID);

    }

    @Override
    protected void onDestroy(){
        cancellaTask();
        super.onDestroy();
    }



}