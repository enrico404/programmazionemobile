package com.example.jobservice_jobinfo;

import android.app.job.JobParameters;
import android.app.job.JobService;
import android.util.Log;
import android.widget.Toast;

public class ServizioBackground extends JobService {

    private DummyWorkThread t;
    private int conta;

    public ServizioBackground(){
        Log.d("TAG", "Called");
    }

    @Override
    public boolean onStartJob(JobParameters params) {
        Toast.makeText(this, "COndizioni verificate, posso schedulare", Toast.LENGTH_LONG).show();
        Log.d("TAG", "Thread in servizio background: "+ Thread.currentThread().getName());

        t = new DummyWorkThread(this, params);
        t.start();

        return true;

    }

    @Override
    public boolean onStopJob(JobParameters params) {
        Toast.makeText(this, "Condizoni non verificate. Interrompo", Toast.LENGTH_LONG).show();
        t.stopRunning();
        return false;
    }
}
