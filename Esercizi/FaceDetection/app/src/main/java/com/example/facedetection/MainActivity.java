package com.example.facedetection;



import androidx.appcompat.app.AppCompatActivity;

import android.app.Activity;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;

import testing.androidfacciadetector.FaceDetectionAndroid;

public class MainActivity extends Activity implements View.OnClickListener {

    private ProgressBar pbar;
    private ImageView image;
    private Button caricaImg;
    private Button rilevaVolti;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        pbar = (ProgressBar) findViewById(R.id.progressBar);
        pbar.setProgress(0);

        image = (ImageView) findViewById(R.id.imageView);
        caricaImg = (Button) findViewById(R.id.button);
        rilevaVolti = (Button) findViewById(R.id.button2);

        caricaImg.setOnClickListener(this);
        rilevaVolti.setOnClickListener(this);

        rilevaVolti.setClickable(false);





    }

    @Override
    public void onClick(View v) {
        Button bottonePremuto = ((Button) v);
        if(bottonePremuto.equals(caricaImg)){
            Bitmap imgBitmap = BitmapFactory.decodeResource(getApplicationContext().getResources(), R.drawable.faces);
            image.setImageBitmap(imgBitmap);

            Log.d("TAG", imgBitmap.getConfig().name());
            rilevaVolti.setClickable(true);

        }
        else {
            FaceDetectionAndroid fd = new FaceDetectionAndroid(image);
            AsyncTaskFaceDetector task = new AsyncTaskFaceDetector(fd, pbar);
            task.execute();

        }
    }
}