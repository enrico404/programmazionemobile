package com.example.facedetection;

import android.media.FaceDetector;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.ProgressBar;

import testing.androidfacciadetector.FaceDetectionAndroid;

public class AsyncTaskFaceDetector extends AsyncTask<Void, Integer, Void> {

    private FaceDetectionAndroid fd;
    private ProgressBar pBar;
    private boolean noErrori;

    public AsyncTaskFaceDetector(FaceDetectionAndroid fd, ProgressBar pBar){
        this.fd = fd;
        this.pBar = pBar;
    }

    @Override
    protected Void doInBackground(Void... voids) {
        this.publishProgress(0);

        noErrori = fd.conversione();
        publishProgress(33);
        if(noErrori){
            fd.detection();
            publishProgress(66);

            fd.displayOutput();
            publishProgress(100);


        }else {
            publishProgress(100);
            Log.e("LOG", "Errore di conversione");
        }

        return null;
    }

    @Override
    protected void onProgressUpdate(Integer... p){pBar.setProgress(p[0]);}

    @Override
    protected void onPostExecute(Void p){
        if(noErrori)
            fd.update();
    }


}
