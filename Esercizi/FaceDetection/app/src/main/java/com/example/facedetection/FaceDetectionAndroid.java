package testing.androidfacciadetector;

import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.PointF;
import android.graphics.drawable.BitmapDrawable;
import android.media.FaceDetector;
import android.util.Log;
import android.widget.ImageView;

public class FaceDetectionAndroid {

    private Bitmap bitmap;
    private Bitmap imageConvertita;
    private int faceCount;
    private FaceDetector.Face[] faces;
    private ImageView img;


    public FaceDetectionAndroid(ImageView img){
        this.bitmap = ((BitmapDrawable)img.getDrawable()).getBitmap();
        this.img = img;
    }

    public boolean conversione(){

        //conversione a diverso formato. La facedetection di Android funziona su immagini di tipo RGB565.
        //RGB565 è un immagine a tre canali (rosso, verde, blu) in cui si usano 5 bit per specificare
        //il rosso ed il blu, ma 6 per il verde. Di default un'immagine presa dalla fotocamera è formato ARGB_8888,
        //ossia 8 bit per quattro canali. Il quarto canale A, è l'alpha, cioè l'eventuale effetto di trasparenza.
        //L'immagine allegata a questo file deve subire lo stesso processo di conversione.

        imageConvertita = bitmap.copy(Bitmap.Config.RGB_565, true);

        if(imageConvertita == null)
            return false;

        //la dimensione lungo x dell'immagine deve essere pari.
        //Questo è un altro vincolo imposto dall'algoritmo di face detection di android.
        if(((imageConvertita.getWidth()%2)!=0))
        {
            //se è dispari, allunghiamo l'immagine di un pixel nella sua lunghezza...
            imageConvertita = Bitmap.createScaledBitmap(imageConvertita,
                    imageConvertita.getWidth()+1, imageConvertita.getHeight(), false);
        }

        return true;
    }

    public int detection(){
        //da qui in poi facciamo partire l'algoritmo di face detection
        //in dotazione sull'android SDK.

        FaceDetector faceDetector = new FaceDetector(
                imageConvertita.getWidth(), imageConvertita.getHeight(),
                10); //quest'ultimo numero indica il numero massimo di faces da rilevare...

        faces = new FaceDetector.Face[10]; //allochiamo spazio per alpiù 10 volti
        //La classe Face è definita nell'SDK di android, così come FaceDetector.
        faceCount = faceDetector.findFaces(imageConvertita, faces);
        //_______________________________________(immagine input, output)

        return faceCount;
    }

    public void displayOutput(){
        //PointF è una struttura dati costituita da un punto bidimensionale
        //le cui dimensioni (x,y) sono costituite da variabili float

        PointF[] inMezzoAgliOcchi = new PointF[faceCount];
        //l'algoritmo di face detection ha come output, le coordinate del punto mediano
        //dell'ipotetico segmento che congiunge i due occhi.
        //Ovviamente abbiamo anche la distanza tra i due occhi.

        Log.v("LOG", "rilevati " + faceCount + " volti");

        for (int i = 0; i < faceCount; i++) //per ogni faccia rilevata
        {

            FaceDetector.Face face = faces[i];
            inMezzoAgliOcchi[i] = new PointF();
            face.getMidPoint(inMezzoAgliOcchi[i]);      //prendi il punto mediano tra i due occhi

            //calcola il quadrato del raggio
            //dell'eventuale cerchio che costruiremo attorno alla faccia.
            //Tale cerchio apparirà come una vetro rosso posto sopra l'immagine di input.

            float raggio2 =  face.eyesDistance() * face.eyesDistance();

            //per ogni pixel all'interno dell'immagine....
            for(int x = 0; x<imageConvertita.getWidth(); x++)
                for(int y=0; y<imageConvertita.getHeight(); y++)
                {

                    //equazione del cerchio. Se questa quantità è minore del raggio al quadrato,
                    //il pixel considerato si colloca all'interno del cerchio rosso.
                    float eqCerchio =     (x-inMezzoAgliOcchi[i].x) *  (x-inMezzoAgliOcchi[i].x)
                            + (y-inMezzoAgliOcchi[i].y) *  (y-inMezzoAgliOcchi[i].y);

                    if( eqCerchio <=  raggio2 )
                    {

                        //nota immagine.getPixel(int, int);
                        //ci permette di salvare su un intero il colore del pixel
                        //alle coordinate dell'immagine specificate come x ed y nei parametri del metodo.

                        //setPixel fa l'operazione inversa. Prende tre parametri, i primi due sono
                        //le coordinate del pixel rispetto all'immagine ed il terzo parametro un colore
                        //espresso come un intero.

                        //l'intero che rappresenta il colore possiamo esplicitarlo bit per bit
                        //oppure usare la classe Color che è presente nell'SDK android.

                        //NOTA BENE: get/setPixel erano presenti anche nello JDK, ma mentre nel JDK
                        //si applicavano ad un oggetto di tipo BufferedImage, qui si applica ad un oggetto Bitmap.
                        //In sostanza non cambia molto, ma vi consiglio di leggervi il javadoc di queste due classi.

                        //settiamo a zero le componenti verdi e blu dei pixel dell'immagine
                        //considerata:
                        int componenteRossa = imageConvertita.getPixel(x, y);
                        componenteRossa = Color.red(componenteRossa); //ora abbiamo solo la componente rossa...
                        imageConvertita.setPixel(x, y, Color.rgb(componenteRossa, 0 ,0)   );
                    }

                }
        }

    }

    //Questo thread non può essere chiamato durante "doInBackground"!!!!
    public void update(){
        img.setImageBitmap(imageConvertita); //riaggiorniamo l'immagine della nostra ImageView
    }

}