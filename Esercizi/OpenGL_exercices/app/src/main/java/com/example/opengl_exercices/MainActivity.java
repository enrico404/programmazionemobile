package com.example.opengl_exercices;

import androidx.appcompat.app.AppCompatActivity;

import android.app.Activity;
import android.app.ActivityManager;
import android.content.Context;
import android.content.pm.ConfigurationInfo;
import android.opengl.GLSurfaceView;
import android.os.Bundle;
import android.util.Log;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.AccelerateInterpolator;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);

        //get reference to the activity manager (AM)
        final ActivityManager activityManager = (ActivityManager) getSystemService(Context.ACTIVITY_SERVICE);
        //from AM get an object with oour mobile device info
        final ConfigurationInfo configurationInfo = activityManager.getDeviceConfigurationInfo();

        boolean isSurfaceCreated = false;

        int supported = 1;

        if(configurationInfo.reqGlEsVersion >= 0x30000)
            supported = 3;
        else if(configurationInfo.reqGlEsVersion >= 0x20000)
            supported = 2;

        Log.v("TAG", "OpenGLES supported >=: "+supported);

        GLSurfaceView surface;

        surface = new GLSurfaceView(this);
        surface.setEGLContextClientVersion(supported);
        surface.setPreserveEGLContextOnPause(true);
        GLSurfaceView.Renderer renderer = new BasicRenderer(0.45f, 0.32f, 0.13f);

        setContentView(surface);
        ((BasicRenderer) renderer).setContextAndSurface(this, surface);
        surface.setRenderer(renderer);
        isSurfaceCreated = true;
    }
}