package com.example.opengl_exercices;

import android.content.Context;
import android.graphics.Point;
import android.opengl.GLSurfaceView;
import android.util.Log;

import javax.microedition.khronos.egl.EGLConfig;
import javax.microedition.khronos.opengles.GL10;
import static android.opengl.GLES20.*;
import static android.opengl.GLES30.*;

public class BasicRenderer implements GLSurfaceView.Renderer {
    //screen color
    protected float clearScreen[];
    //current screen size
    protected Point currentScreen;
    //retference to the app context
    protected Context context;
    //reference to the actual surface
    protected GLSurfaceView surface;
    //protected static String TAG;


    public BasicRenderer(){
        this(0,0,0);
    }

    public BasicRenderer(float r, float g, float b){
        this(r, g, b, 1);
    }

    public BasicRenderer(float r, float g, float b, float a){
        //TAG = getClass().getSimpleName();
        clearScreen = new float[]{r,g,b,a};
        currentScreen = new Point(0,0);

    }

    public void setContextAndSurface(Context context, GLSurfaceView surface) {
        this.context = context;
        this.surface = surface;
    }

    public Context getContext() {
        return context;
    }

    public GLSurfaceView getSurface() {
        return surface;
    }

    @Override
    public void onSurfaceCreated(GL10 gl, EGLConfig config) {
        glClearColor(clearScreen[0], clearScreen[1], clearScreen[2], clearScreen[3]);
        Log.v("TAG", "onSurfaceCreated "+ Thread.currentThread().getName());
        Log.v("TAG", glGetString(GL_VERSION));

    }

    @Override
    public void onSurfaceChanged(GL10 gl, int w, int h) {
        Log.v("TAG", "onSurfaceChanged "+ Thread.currentThread().getName());
        glViewport(0,0, w, h);
        currentScreen.x = w;
        currentScreen.y = h;

    }

    @Override
    public void onDrawFrame(GL10 gl) {
        glClear(GL_COLOR_BUFFER_BIT);

    }


}
