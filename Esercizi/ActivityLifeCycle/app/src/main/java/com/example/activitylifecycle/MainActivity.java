package com.example.activitylifecycle;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.util.Log;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {
    private Bundle b;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Log.d("TAG", "On Create!");
        b = savedInstanceState;
        if (b == null){
            b = new Bundle();
            b.putInt("Conta", 0);
        }

    }

    @Override
    protected void onStart() {
        super.onStart();
        Log.d("TAG", "On Start!");
        int conta;
        conta = b.getInt("Conta");
        conta++;
        b.putInt("Conta", conta);

    }

    @Override
    protected void onStop() {
        super.onStop();
        Log.d("TAG", "On Stop!");
    }


    @Override
    protected void onPause() {
        super.onPause();
        Log.d("TAG", "On Pause!");
    }


    @Override
    protected void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putInt("Conta", b.getInt("Conta"));

    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.d("TAG", "On Resume!");
        TextView t = (TextView) findViewById(R.id.text);
        t.setText("Conta: "+ b.getInt("Conta"));
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        Log.d("TAG", "On Restart!");
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Log.d("TAG", "On Destroy!");
    }
}