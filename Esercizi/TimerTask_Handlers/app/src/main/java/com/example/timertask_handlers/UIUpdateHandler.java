package com.example.timertask_handlers;

import android.os.Bundle;
import android.os.Handler;
import android.os.Message;


import java.util.logging.LogRecord;

public class UIUpdateHandler extends Handler {

    private MainActivity activity;

    public UIUpdateHandler(MainActivity activity){
        this.activity = activity;
    }

    @Override
    public void handleMessage(Message msg){
        Bundle b = msg.getData();

        if(b != null){
            activity.updateUI(b.getInt("posX"), b.getInt("posY"));
        }
    }

}
