package com.example.timertask_handlers;

import androidx.appcompat.app.AppCompatActivity;

import android.app.Activity;
import android.os.Bundle;
import android.text.style.AbsoluteSizeSpan;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsoluteLayout;
import android.widget.ImageView;

import java.util.Timer;

public class MainActivity extends Activity implements View.OnTouchListener {

    private ImageView img;
    private Timer timer;
    private TimerMovimento tm;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        img = findViewById(R.id.ball);
        UIUpdateHandler handler = new UIUpdateHandler(this);
        AbsoluteLayout.LayoutParams params = (AbsoluteLayout.LayoutParams) img.getLayoutParams();
        AbsoluteLayout layout = findViewById(R.id.layout);
        layout.setOnTouchListener(this);


        timer = new Timer();
        tm = new TimerMovimento(handler, params.x, params.y);
        timer.scheduleAtFixedRate(tm, 10, 10);


    }

    @Override
    public boolean onTouch(View v, MotionEvent event) {
        tm.setPosizioneFutura((int) event.getX(), (int) event.getY());
        return true;
    }

    protected void updateUI(int x, int y){
        AbsoluteLayout.LayoutParams params = (AbsoluteLayout.LayoutParams) img.getLayoutParams();
        params.x = x;
        params.y = y;
        img.setLayoutParams(params);

    }

    @Override
    public void onStop(){
        super.onStop();
        tm.addormenta();
    }

    @Override
    public void onStart(){
        super.onStart();
        tm.sveglia();
    }


    @Override
    public void onDestroy(){
       super.onDestroy();
       tm.cancel();
       timer.purge();
       timer.cancel();;


    }

}