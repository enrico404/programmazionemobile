package com.example.timertask_handlers;

import android.graphics.Point;
import android.os.Bundle;
import android.os.Message;

import java.util.TimerTask;

public class TimerMovimento extends TimerTask {

    private UIUpdateHandler handler;
    private Point currentPos;
    private Point futurePos;
    private boolean sveglio;
    private Object lock;

    public TimerMovimento(UIUpdateHandler handler, int xInit, int yInit) {
        this.handler = handler;
        this.currentPos = new Point(xInit, yInit);
        this.futurePos = new Point(-1, -1);
        sveglio = true;
        lock = new Object();

    }

    public void setPosizioneFutura(int x, int y){
        futurePos.x = x;
        futurePos.y = y;
    }


    public boolean arrivato(){
        if(Math.abs(futurePos.x-currentPos.x) < 6 && Math.abs(futurePos.y - currentPos.y) < 6)
            return true;
        if(futurePos.x == -1 || futurePos.y == -1)
            return true;
        return false;

    }

    @Override
    public void run() {
        if(!arrivato()){
            if(currentPos.x < futurePos.x){
                currentPos.x += 5;
            }else if (currentPos.x > currentPos.x){
                currentPos.x -= 5;
            }

            if(currentPos.y < futurePos.y){
                currentPos.y += 5;
            }else if (currentPos.y > currentPos.y){
                currentPos.y -= 5;
            }

            Message m = handler.obtainMessage();
            Bundle b = m.getData();
            b.putInt("posX", currentPos.x);
            b.putInt("posY", currentPos.y);
        }
        else {
            synchronized (lock) {
                try{
                    lock.wait();
                }catch (InterruptedException e){}
            }

        }

    }


    public void addormenta(){
        handler.removeCallbacksAndMessages(null);
        futurePos.x = -1;
        futurePos.y = -1;
        synchronized (lock){
            lock.notify();
        }
    }

    public void sveglia(){
        sveglio = true;
        handler.removeCallbacksAndMessages(null);
        futurePos.x = -1;
        futurePos.y = -1;
        sveglio = false;
    }
}
