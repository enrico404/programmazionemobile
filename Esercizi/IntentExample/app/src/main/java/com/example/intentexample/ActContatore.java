package com.example.intentexample;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

public class ActContatore extends AppCompatActivity implements View.OnClickListener {
    private int cont;
    private TextView text;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_contatore);
        text = findViewById(R.id.contatore);

        Button chiudi = findViewById(R.id.bChiudi);
        Button inc = findViewById(R.id.bInc);
        Button dec = findViewById(R.id.bDec);

        chiudi.setOnClickListener(this);
        inc.setOnClickListener(this);
        dec.setOnClickListener(this);


        if(savedInstanceState == null){
            cont = Integer.parseInt((String)text.getText());
        }
        else
            cont = savedInstanceState.getInt("cont");
        text.setText(cont+"");
        String msg = "il contatore vale: "+cont;
        Log.d("TAG", msg);
    }

    @Override
    public void onClick(View v) {
        String label = (String) ((Button)v).getText();
        if(label.equals("Decrementa")){
            cont -=  1;
            text.setText(""+cont);
        }
        else if (label.equals("Incrementa")){
            cont += 1;
            text.setText(""+cont);
        }
        //è il chiudi
        else  {
            Intent i = new Intent();
            i.putExtra("cont", cont);
            setResult(RESULT_OK, i);
            super.finish();
        }

    }
}
