package com.example.intentexample;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.SeekBar;

import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.widget.ConstraintLayout;

public class ActCambioCol extends AppCompatActivity implements View.OnClickListener, SeekBar.OnSeekBarChangeListener {

    private SeekBar sRed;
    private SeekBar sGreen;
    private SeekBar sBlue;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cambio_colore);
        Button bCambiaCol = findViewById(R.id.bCambiaCol);
        bCambiaCol.setOnClickListener(this);
        sRed = findViewById(R.id.seekBar);
        sGreen = findViewById(R.id.seekBar2);
        sBlue = findViewById(R.id.seekBar3);

        sRed.setOnSeekBarChangeListener(this);
        sGreen.setOnSeekBarChangeListener(this);
        sBlue.setOnSeekBarChangeListener(this);

    }

    public void cambioCol(){

        ConstraintLayout baseLayout = findViewById(R.id.baseLayout);
        baseLayout.setBackgroundColor(Color.rgb(sRed.getProgress(), sGreen.getProgress(), sBlue.getProgress()));
    }



    @Override
    public void onClick(View v) {
        String label = (String) ((Button)v).getText();
        Log.d("TAG", "sono qua");
        if(label.equals("Cambia colore")){


            Intent i = new Intent();
            i.putExtra("bR", sRed.getProgress());
            i.putExtra("bG", sGreen.getProgress());
            i.putExtra("bB", sBlue.getProgress());
            setResult(RESULT_OK, i);
            super.finish();
        }
    }

    @Override
    public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
        cambioCol();
    }

    @Override
    public void onStartTrackingTouch(SeekBar seekBar) {
        
    }

    @Override
    public void onStopTrackingTouch(SeekBar seekBar) {

    }
}
