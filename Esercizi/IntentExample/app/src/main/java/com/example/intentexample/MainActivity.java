package com.example.intentexample;

import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.widget.ConstraintLayout;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity implements View.OnClickListener{

    private TextView text;
    private static final int REQUEST_CODE = 1;
    private static final int CHANGE_BACKGROUND_CODE = 2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        text = findViewById(R.id.text);
        Button bActCont = findViewById(R.id.actCont);
        Button bCambiaCol = findViewById(R.id.actCambiaCol);
        bActCont.setOnClickListener(this);
        bCambiaCol.setOnClickListener(this);
        if (savedInstanceState != null){
            int bR = savedInstanceState.getInt("bR");
            int bG = savedInstanceState.getInt("bG");
            int bB = savedInstanceState.getInt("bB");
            int cont = savedInstanceState.getInt("cont");
            text.setText("Contatore: "+cont);

        }


    }

    @Override
    public void onClick(View v) {
        String label = (String) ((Button)v).getText();

        if(label.equals("Activity Contatore")){
            Log.d("TAG", "primo bottone cliccato");
            Intent i = new Intent(this, ActContatore.class);
            this.startActivityForResult(i, REQUEST_CODE);

        }
        else if (label.equals("Activity cambio colore")){
            Log.d("TAG", "secondo bottone cliccato");
            Intent i = new Intent(this, ActCambioCol.class);
            this.startActivityForResult(i, CHANGE_BACKGROUND_CODE);

        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK && requestCode == REQUEST_CODE) {
            int conta = data.getExtras().getInt("cont");
            text.setText("Contatore: "+conta);

        }
        else if (resultCode == RESULT_OK && requestCode == CHANGE_BACKGROUND_CODE) {
            int bR = data.getExtras().getInt("bR");
            int bG = data.getExtras().getInt("bG");
            int bB = data.getExtras().getInt("bB");

            RelativeLayout base = findViewById(R.id.baseLayout);
            base.setBackgroundColor(Color.rgb(bR, bG, bB));

        }

    }
}