package com.example.gestureadvanced;

import androidx.appcompat.app.AppCompatActivity;

import android.graphics.Color;
import android.graphics.Point;
import android.os.Bundle;
import android.view.Display;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

import java.util.Random;

public class MainActivity extends AppCompatActivity implements GestureDetector.OnGestureListener, GestureDetector.OnDoubleTapListener {

    private GestureDetector mDetector;
    private RelativeLayout rlUp;
    private RelativeLayout rlDown;


    //random number generator
    private Random rnd;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mDetector = new GestureDetector(this, this);
        mDetector.setOnDoubleTapListener(this);

        rlDown = findViewById(R.id.rlDown);
        rlUp = findViewById(R.id.rlUP);

        //get display size
        Display display = getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        int width = size.x;
        int height = size.y;

        ViewGroup.LayoutParams p = rlDown.getLayoutParams();
        p.height = height/2;
        rlDown.setLayoutParams(p);

        p = rlUp.getLayoutParams();
        p.height = height/2;
        rlUp.setLayoutParams(p);


        rnd = new Random(System.currentTimeMillis());
    }

    @Override
    public boolean onTouchEvent(MotionEvent event){
        this.mDetector.onTouchEvent(event);
        return super.onTouchEvent(event);
    }

    @Override
    public boolean onSingleTapConfirmed(MotionEvent e) {
        int color = Color.rgb(rnd.nextInt(256), rnd.nextInt(256), rnd.nextInt(256));

        if(rnd.nextBoolean()){
            rlUp.setBackgroundColor(color);
        }else rlDown.setBackgroundColor(color);

        return false;
    }

    @Override
    public boolean onDoubleTap(MotionEvent e) {
        int color = Color.rgb(rnd.nextInt(256), rnd.nextInt(256), rnd.nextInt(256));
        int color2 = Color.rgb(rnd.nextInt(256), rnd.nextInt(256), rnd.nextInt(256));
        rlUp.setBackgroundColor(color);
        rlDown.setBackgroundColor(color2);
        return false;
    }

    @Override
    public boolean onDoubleTapEvent(MotionEvent e) {
        return false;
    }

    @Override
    public boolean onDown(MotionEvent e) {
        return false;
    }

    @Override
    public void onShowPress(MotionEvent e) {

    }

    @Override
    public boolean onSingleTapUp(MotionEvent e) {
        return false;
    }

    @Override
    public boolean onScroll(MotionEvent e1, MotionEvent e2, float distanceX, float distanceY) {
        ViewGroup.LayoutParams pUp = rlUp.getLayoutParams();
        ViewGroup.LayoutParams pDown = rlDown.getLayoutParams();

        float yStart = e1.getY();
        float yEnd = e2.getY();

        if(yEnd > yStart){
            //alto verso basso
            pDown.height -= 10;
            pUp.height += 10;

        }
        else {
            //basso verso alto
            pDown.height += 10;
            pUp.height -= 10;

        }

        rlUp.setLayoutParams(pUp);
        rlDown.setLayoutParams(pDown);
        return false;
    }

    @Override
    public void onLongPress(MotionEvent e) {

    }

    @Override
    public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX, float velocityY) {
        return false;
    }
}