package com.example.testlayout;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.Button;
import android.widget.LinearLayout;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        LinearLayout ll = findViewById(R.id.layoutLL);

        Button b0 = new Button(this);
        Button b1 = new Button(this);
        Button b2 = new Button(this);

        ll.addView(b0);
        ll.addView(b1);
        ll.addView(b2);

        b0.setText("Bottone0");
        b1.setText("Bottone1");
        b2.setText("Bottone2");

    }
}