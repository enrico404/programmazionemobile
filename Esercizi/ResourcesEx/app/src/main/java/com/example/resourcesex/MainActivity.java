package com.example.resourcesex;

import androidx.appcompat.app.AppCompatActivity;

import android.app.ActivityManager;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.util.Log;
import android.view.WindowManager;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        TextView t = findViewById(R.id.text);

        //ovveride della lingua
        /*
        Configuration configuration = getResources().getConfiguration();
        configuration.setLocale(Locale.ITALIAN);
        getResources().updateConfiguration(configuration, getResources().getDisplayMetrics());

         */

        t.setText(R.string.message);


        //gioco un po' con i manager

        ActivityManager am = (ActivityManager) getSystemService(ACTIVITY_SERVICE);
        for(ActivityManager.RunningAppProcessInfo i : am.getRunningAppProcesses()){
            Log.v("TAG", i.processName);
        }

        PackageManager pm = getPackageManager();
        List<PackageInfo> list = pm.getInstalledPackages(0);
        ArrayList<PackageInfo> listA = new ArrayList<>(list);
        for (PackageInfo pi : listA)
            Log.d("TAG", pi.packageName);

        WindowManager wm = getWindowManager();
        Log.d("TAG", "Refresh rate: "+wm.getDefaultDisplay().getRefreshRate());

        ConnectivityManager cm = (ConnectivityManager) getSystemService(CONNECTIVITY_SERVICE);
        String type = "No connection";
        if(cm.getActiveNetworkInfo() != null){
            type = cm.getActiveNetworkInfo().getTypeName();
        }
        Log.d("TAG", "Connection: "+type);


    }

    @Override
    protected void onStart() {
        super.onStart();

        Toast toast = new Toast(this);
        toast.makeText(this, R.string.msg2, Toast.LENGTH_LONG).show();

    }
}