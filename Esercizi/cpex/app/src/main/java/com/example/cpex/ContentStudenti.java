package com.example.cpex;

import android.content.ContentProvider;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.DatabaseUtils;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.database.sqlite.SQLiteQueryBuilder;
import android.net.Uri;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import java.util.LinkedHashSet;
import java.util.Map;
import java.util.Set;

public class ContentStudenti extends ContentProvider {
    static final String PROVIDER_NAME = "com.example.cpex.ContentStudenti";
    static final String URL = "content://"+PROVIDER_NAME+"/studenti";
    static final Uri CONTENT_URI = Uri.parse(URL);


    //colonne della tabella studenti
    static final String NOME = "nome";
    static final String CORSO = "corso";
    static final String MEDIA_ATTUALE = "media";

    //matching tra colonna e nome inserito dall'utente
    private static Map<String, String> mappaStudenti;

    //reference al db
    private SQLiteDatabase db;


    static final String DATABASE_NAME = "Universita";
    static final String STUDENTS_TABLE_NAME = "studenti";
    static final int DATABASE_VERSION = 4;

    //query di creazione della tabella
    static final String CREATE_DB_TABLE2 = "CREATE TABLE "+ STUDENTS_TABLE_NAME +
             "( " + NOME + " TEXT PRIMARY KEY, "+
             CORSO +" TEXT NOT NULL, "+
             MEDIA_ATTUALE +" INTEGER);";

    static final String CREATE_DB_TABLE =
            " CREATE TABLE " + STUDENTS_TABLE_NAME +
                    " (nome TEXT PRIMARY KEY, " +
                    " corso TEXT NOT NULL," +
                    " media INTEGER"+
                    ");";


    private SQLiteQueryBuilder qb;

    private static class DBWrapper extends SQLiteOpenHelper {

        DBWrapper(Context context){
            super(context, DATABASE_NAME, null, DATABASE_VERSION);
        }

        @Override
        public void onCreate(SQLiteDatabase db) {
            db.execSQL(CREATE_DB_TABLE);
            Log.d("TAG", "db creato");
        }

        @Override
        public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
            db.execSQL("DROP TABLE IF EXISTS "+ STUDENTS_TABLE_NAME);
            onCreate(db);
        }
    }

    private DBWrapper dbHelper;
    @Override
    public boolean onCreate() {
        Context context = getContext();
        dbHelper = new DBWrapper(context);
        db = dbHelper.getWritableDatabase();
        qb = new SQLiteQueryBuilder();
        qb.setProjectionMap(mappaStudenti);
        //setto come univa tabella al query builder, la tabella studenti
        qb.setTables(STUDENTS_TABLE_NAME);
        Log.d("TAG", "db settato");

        return (db == null) ? false : true;

    }

    @Nullable
    @Override
    public Cursor query(@NonNull Uri uri, @Nullable String[] projection, @Nullable String selection, @Nullable String[] selectionArgs, @Nullable String sortOrder) {
        Cursor c = qb.query(db, projection, selection, selectionArgs, null, null, sortOrder);
        return c;
    }

    @Nullable
    @Override
    public String getType(@NonNull Uri uri) {
        //di che tipo sono le entry del mio db?

        return "com.example.cpex.Studente";
    }

    @Nullable
    @Override
    public Uri insert(@NonNull Uri uri, @Nullable ContentValues values) {
        //come inserire un nuovo record di studente
        long rowID = -1;
        rowID = db.insert(STUDENTS_TABLE_NAME, "", values);
        if(rowID > 0 ){
            //inserito correttamente
            Uri uriOut = ContentUris.withAppendedId(CONTENT_URI, rowID);
            return uriOut;
        }
        else {
            //errore nell'inserimento
            Log.d("TAG", "Errore nell'inserimento, entry già esistente!");
        }
        return null;
    }

    @Override
    public int delete(@NonNull Uri uri, @Nullable String selection, @Nullable String[] selectionArgs) {
        //come cancellare

        return db.delete(STUDENTS_TABLE_NAME, selection, selectionArgs);
    }

    @Override
    public int update(@NonNull Uri uri, @Nullable ContentValues values, @Nullable String selection, @Nullable String[] selectionArgs) {
        //come fare l'update del db
        return db.update(STUDENTS_TABLE_NAME, values, selection, selectionArgs);
    }


    //metodi aggiuntivi di cortesia

    public long contaElementi(){
        return DatabaseUtils.queryNumEntries(db, STUDENTS_TABLE_NAME);
    }

    public Set<Studente> ordinaStudenti(){
        Set<Studente> out = new LinkedHashSet<>();
        final String QUERY = "select * from "+ STUDENTS_TABLE_NAME+ " order by "+ MEDIA_ATTUALE+ " desc ";
        Cursor c = db.rawQuery(QUERY, null);

        if(c != null){
            c.moveToFirst();
            int indexNome = c.getColumnIndex(NOME);
            int indexCorso = c.getColumnIndex(CORSO);
            int indexMedia = c.getColumnIndex(MEDIA_ATTUALE);
            if(c.getCount() > 0){
                do{
                    out.add(new Studente(c.getString(indexNome), c.getString(indexCorso), c.getInt(indexMedia)));
                }while(c.moveToNext());
            }
        }
        return out;

    }

    public SQLiteDatabase getRawDB(){
        return db;
    }

    public void debugContent(){
        Cursor c = db.rawQuery("select * from "+STUDENTS_TABLE_NAME+ " order by "+MEDIA_ATTUALE+ " desc ", null);
        if(c != null){
            c.moveToFirst();
            do {
                String s = "";
                for (int i=0; i< c.getColumnCount(); i++){
                    s += " || "+ c.getString(i);
                }
                Log.d("TAG", s);
            }while(c.moveToNext());
        }

    }



}
