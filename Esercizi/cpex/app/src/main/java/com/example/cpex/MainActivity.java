package com.example.cpex;

import androidx.appcompat.app.AppCompatActivity;

import android.Manifest;
import android.app.Activity;
import android.content.ContentProviderClient;
import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class MainActivity extends Activity {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);



        ContentResolver resolver = this.getContentResolver();
        ContentProviderClient client = resolver.acquireContentProviderClient(ContentStudenti.CONTENT_URI);
        ContentStudenti contentStudenti = (ContentStudenti) client.getLocalContentProvider();

        if (contentStudenti.contaElementi() == 0) {
            //se il db è vuoto lo creo

            List<Studente> studenti = new ArrayList<>();
            Random rnd = new Random(System.currentTimeMillis());
            Log.d("TAG", "DB vuoto. Lo creo ed inserisco elementi");
            for (int i = 0; i < 10; i++) {

                studenti.add(new Studente("Studente" + i, "LMInfo", (rnd.nextInt(30 - 18) + 18)));
            }
            ContentValues values = new ContentValues();
            for (Studente st : studenti) {
                values.put(ContentStudenti.NOME, st.getNome());
                values.put(ContentStudenti.CORSO, st.getCorso());
                values.put(ContentStudenti.MEDIA_ATTUALE, st.getMediaAttuale());
                getContentResolver().insert(ContentStudenti.CONTENT_URI, values);
            }

        } else {
            //se è già pieno lo devo stamapare
            contentStudenti.debugContent();
        }



        //estraggo uno studente specifico, devo usare la projection

        String[] projection = {ContentStudenti.CORSO, ContentStudenti.MEDIA_ATTUALE};
        String selClause = ContentStudenti.NOME + "= ?";
        Cursor c = resolver.query(ContentStudenti.CONTENT_URI, projection, selClause, new String[]{"Studente2"}, null);

        if (c != null) {
            if (c.getCount() > 0) {

                do {
                    c.moveToFirst();
                    int index = c.getColumnIndex(ContentStudenti.CORSO);
                    String corso = c.getString(index);
                    index = c.getColumnIndex(ContentStudenti.MEDIA_ATTUALE);
                    int media = c.getInt(index);
                    Log.d("TAG", "Studente2 iscritto a " + corso + " ha media attuale di " + media );


                } while (c.moveToNext());

            }

        }else Log.d("TAG", "la query non ha risultati");

        for(Studente studente : contentStudenti.ordinaStudenti()){
            Log.d("TAG", studente.toString());
        }

    }

    @Override
    protected void onStart() {
        super.onStart();


    }
}