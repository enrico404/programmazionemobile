package com.example.cpex;

public class Studente {
    private String nome;
    private String corso;
    private int mediaAttuale;

    //Constructor
    public Studente(String nome, String corso, int mediaAttuale) {
        this.nome = nome;
        this.corso = corso;
        this.mediaAttuale = mediaAttuale;
    }

    public String getNome() {
        return nome;
    }

    public String getCorso() {
        return corso;
    }

    public int getMediaAttuale() {
        return mediaAttuale;
    }
    @Override
    public String toString(){
        return nome + " " + corso + " " + mediaAttuale;
    }


}
