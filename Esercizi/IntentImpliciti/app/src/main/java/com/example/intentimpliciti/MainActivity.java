package com.example.intentimpliciti;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity implements TextWatcher, View.OnClickListener {

    private Button button;
    private ImageView img;
    private EditText editText;
    private static final int REQ_CODE = 1;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        button = findViewById(R.id.button);
        img = findViewById(R.id.imageView3);
        editText = findViewById(R.id.editText);
        String text = editText.getText().toString();
        if(text.startsWith("http") || text.startsWith("www")) {
            button.setText("Apri Browser");
        }
        else{
            button.setText("Scatta foto");
        }



        editText.addTextChangedListener(this);
        button.setOnClickListener(this);


    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {
        Log.d("TAG", "testo cambiato");
        String text = editText.getText().toString();
        if(text.startsWith("http") || text.startsWith("www")) {
            button.setText("Apri Browser");
        }
        else{
            button.setText("Scatta foto");
        }


    }

    @Override
    public void afterTextChanged(Editable s) {

    }

    @Override
    public void onClick(View v) {
        String label = (String) ((Button)v).getText();
        if(label.equals("Scatta foto")){
            Log.d("TAG", "Scatta foto");
            Intent i = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
            this.startActivityForResult(i, REQ_CODE);
        }else{
            Log.d("TAG", "Apri in browser");
            Uri uri = Uri.parse(editText.getText().toString());
            Intent i = new Intent(Intent.ACTION_VIEW, uri);
            startActivity(i);
        }
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode == REQ_CODE){
            Bundle extras = data.getExtras();
            Bitmap imgBitmap = (Bitmap) extras.get("data");
            img.setImageBitmap(imgBitmap);
        }
        else
            Toast.makeText(this, "Foto non scattata", Toast.LENGTH_LONG).show();

    }

}