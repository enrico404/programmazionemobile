package com.example.gestures;

import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.widget.ConstraintSet;

import android.os.Bundle;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity implements GestureDetector.OnGestureListener, GestureDetector.OnDoubleTapListener {

    private TextView text;
    private GestureDetector mDetector;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mDetector = new GestureDetector(this, this);
        text = findViewById(R.id.text);

        mDetector.setOnDoubleTapListener(this);


    }

     @Override
     public boolean onTouchEvent(MotionEvent event){
        this.mDetector.onTouchEvent(event);
        return super.onTouchEvent(event);
     }

    @Override
    public boolean onSingleTapConfirmed(MotionEvent e) {
        text.setText("On single tap confirmed");
        return false;
    }

    @Override
    public boolean onDoubleTap(MotionEvent e) {
        text.setText("On double tap");
        return false;
    }

    @Override
    public boolean onDoubleTapEvent(MotionEvent e) {
        text.setText("On double tap event");
        return false;
    }

    @Override
    public boolean onDown(MotionEvent e) {
        text.setText("On down");
        return false;
    }

    @Override
    public void onShowPress(MotionEvent e) {
        text.setText("On show press");

    }

    @Override
    public boolean onSingleTapUp(MotionEvent e) {
        text.setText("On single tap up");
        return false;
    }

    @Override
    public boolean onScroll(MotionEvent e1, MotionEvent e2, float distanceX, float distanceY) {
        text.setText("On scroll");
        return false;
    }

    @Override
    public void onLongPress(MotionEvent e) {
        text.setText("On long press");
    }

    @Override
    public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX, float velocityY) {
        text.setText("On fling");
        return false;
    }
}